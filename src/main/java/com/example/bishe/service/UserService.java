package com.example.bishe.service;

import com.example.bishe.mapper.UserMapper;
import com.example.bishe.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    public User getUseInfo(String username) {
        Example example = new Example(User.class);
        example.createCriteria().andEqualTo("username", username);
        List<User> users = userMapper.selectByExample(example);

        if (!CollectionUtils.isEmpty(users)) {
            return users.get(0);
        }

        return null;
    }

    public void insertUser(User user) {
        user.setIsLandlord(0);
        userMapper.insertSelective(user);
    }

    public void updateUser(User user) {
        userMapper.updateByPrimaryKeySelective(user);
    }

    public User getUserById(Long userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    public void updateUserLandlord(Long id) {
        User user = new User();
        user.setId(id);
        user.setIsLandlord(1);
        userMapper.updateByPrimaryKeySelective(user);
    }
}
