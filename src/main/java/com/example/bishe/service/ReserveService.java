package com.example.bishe.service;

import com.example.bishe.mapper.HouseMapper;
import com.example.bishe.mapper.ReserveMapper;
import com.example.bishe.mapper.UserMapper;
import com.example.bishe.pojo.House;
import com.example.bishe.pojo.Reserve;
import com.example.bishe.utils.DateUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.websocket.server.ServerEndpoint;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReserveService {

    @Autowired
    ReserveMapper reserveMapper;

    @Autowired
    HouseMapper houseMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    HouseOrderService hoseOrderService;

    @Autowired
    HouseService houseService;

    public Boolean isReServeHouse(Long userId, Long houseId) {
        Example example = new Example(Reserve.class);
        example.createCriteria().andEqualTo("userId", userId).andEqualTo("houseId", houseId).andEqualTo("isExpired", 0);

        List<Reserve> reserves = reserveMapper.selectByExample(example);

        return reserves.size() > 0;
    }

    public void reserveHouse(Long userId, Long houseId, Date startDate, Date endDate) {
        Reserve reserve = new Reserve();
        reserve.setUserId(userId);
        reserve.setHouseId(houseId);
        reserve.setStartTime(startDate);
        reserve.setEndTime(endDate);
        reserve.setIsExpired(0);

        reserveMapper.insertSelective(reserve);

    }

    public List<Reserve> getReserves(Long id, Integer page, Integer pageSize) {

        List<Long> houseIds = houseMapper.getHouseIds(id);

        if (houseIds == null || houseIds.size() == 0) {
            return new ArrayList<>();
        }

        Example example = new Example(Reserve.class);
        example.setOrderByClause("id DESC");

        example.createCriteria().andIn("houseId", houseIds);
        PageHelper.startPage(page, pageSize);

        List<Reserve> reserves = reserveMapper.selectByExample(example);

        return reserves.stream().peek(reserve -> {
            reserve.setUsername(userMapper.selectByPrimaryKey(reserve.getUserId()).getUsername());
            reserve.setHouse(houseService.getHouse(reserve.getHouseId()));
            try {
                reserve.setStartTimeInfo(DateUtils.format(reserve.getStartTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                reserve.setEndTimeInfo(DateUtils.format(reserve.getEndTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }).collect(Collectors.toList());
    }

    public List<Reserve> getReservesByUid(Long id, Integer page, Integer pageSize) {


        Example example = new Example(Reserve.class);
        example.setOrderByClause("id DESC");

        example.createCriteria().andEqualTo("userId", id);
        PageHelper.startPage(page, pageSize);

        List<Reserve> reserves = reserveMapper.selectByExample(example);

        return reserves.stream().peek(reserve -> {
            reserve.setUsername(userMapper.selectByPrimaryKey(reserve.getUserId()).getUsername());
            reserve.setHouse(houseService.getHouse(reserve.getHouseId()));
            try {
                reserve.setStartTimeInfo(DateUtils.format(reserve.getStartTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                reserve.setEndTimeInfo(DateUtils.format(reserve.getEndTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }).collect(Collectors.toList());
    }

    public int getTotal(Long id) {
        List<Long> houseIds = houseMapper.getHouseIds(id);

        if (houseIds == null || houseIds.size() == 0) {
            return 0;
        }

        Example example = new Example(Reserve.class);
        example.createCriteria().andIn("houseId", houseIds);

        return reserveMapper.selectCountByExample(example);
    }

    public int getTotalByUid(Long id) {


        Example example = new Example(Reserve.class);
        example.createCriteria().andEqualTo("userId", id);

        return reserveMapper.selectCountByExample(example);
    }

    public void agree(Long reserveId) {

        Reserve reserve = reserveMapper.selectByPrimaryKey(reserveId);
        reserve.setIsExpired(1);
        reserveMapper.updateByPrimaryKeySelective(reserve);

        Reserve reserve1 = new Reserve();
        reserve1.setIsExpired(2);

        Example example = new Example(Reserve.class);
        example.createCriteria().andEqualTo("houseId", reserve.getHouseId()).
                andNotEqualTo("id", reserveId).andEqualTo("isExpired", 0);
        reserveMapper.updateByExampleSelective(reserve1, example);

        hoseOrderService.insert(reserve);

        House house = new House();
        house.setId(reserve.getHouseId());
        house.setIsRent(1);

        houseMapper.updateByPrimaryKeySelective(house);

    }

    public void decline(Long reserveId) {
        Reserve reserve1 = new Reserve();
        reserve1.setIsExpired(2);
        reserve1.setId(reserveId);

        reserveMapper.updateByPrimaryKeySelective(reserve1);
    }
}
