package com.example.bishe.service;

import com.example.bishe.mapper.CityMapper;
import com.example.bishe.mapper.HouseMapper;
import com.example.bishe.pojo.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {

    @Autowired
    CityMapper cityMapper;

    @Autowired
    HouseService houseService;

    public List<City> getAll() {
        return cityMapper.selectAll();
    }

    public City getCityById(Long id) {
        return cityMapper.selectByPrimaryKey(id);
    }

    public List<City> getAllWithNum() {

        List<City> cities = cityMapper.selectAll();
        cities.forEach(city -> {
            city.setNum(houseService.houseNumInCity(city.getId()));

        });
        return cities;
    }

}
