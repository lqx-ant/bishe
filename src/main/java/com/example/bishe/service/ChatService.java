package com.example.bishe.service;

import com.example.bishe.mapper.ChatMapper;
import com.example.bishe.mapper.UserMapper;
import com.example.bishe.pojo.Chat;
import com.example.bishe.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.weekend.Fn;

import javax.xml.crypto.Data;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class ChatService {

    @Autowired
    ChatMapper chatMapper;

    @Autowired
    UserMapper userMapper;

    public List<User> findChatUsers(Long userId) {
        Set<Long> users = new HashSet<>();
        users.addAll(chatMapper.findToUserByUserId(userId));
        users.addAll(chatMapper.findFromUserByUserId(userId));

        return users.stream().map(id -> userMapper.selectByPrimaryKey(id)).collect(Collectors.toList());
    }

    public List<Chat> findByUserId(Long userId, Long chatUserId) {


        List<Chat> ch1 = this.findToChatByUserId(userId, chatUserId);
        ch1.addAll(this.findToChatByUserId(chatUserId, userId));
        ch1.sort(new Comparator<Chat>() {
            @Override
            public int compare(Chat o1, Chat o2) {
                Date d1 = o1.getDate();
                Date d2 = o2.getDate();

                if (d1.before(d2)) {
                    return -1;
                }
                return 1;
            }
        });

        return ch1;
    }

    private List<Chat> findToChatByUserId(Long fromUserId, Long toUserId) {
        Example example = new Example(Chat.class);
        example.createCriteria().andEqualTo("fromUser", fromUserId).andEqualTo("toUser", toUserId);
        return chatMapper.selectByExample(example);
    }

    public int getUnReadNum(Long id) {
        return chatMapper.getUnReadNum(id);
    }

    public void updateUnRead(Long id) {
        chatMapper.updateUnRead(id);
    }

    public void sentMessage(Long id, Long toUserId, String message) {
        Chat chat = new Chat();
        chat.setMessage(message);
        chat.setFromUser(id);
        chat.setToUser(toUserId);
        chat.setIsRead(0);
        chat.setDate(new Date());

        chatMapper.insertSelective(chat);
    }
}
