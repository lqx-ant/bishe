package com.example.bishe.service;

import com.aliyun.oss.OSSClient;
import com.example.bishe.config.AliyunConfig;
import com.example.bishe.pojo.PicUploadResult;
import com.example.bishe.utils.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

@Service
public class UploadPicService {
    // 允许上传的格式
    private static final String[] IMAGE_TYPE = new String[]{".bmp", ".jpg", ".jpeg", ".gif", ".png"};

    @Autowired
    private OSSClient ossClient;

    @Autowired
    private AliyunConfig aliyunConfig;

    public PicUploadResult upload(MultipartFile multipartFile) {
        // 1. 对上传的图片进行校验: 这里简单校验后缀名
        // 另外可通过ImageIO读取图片的长宽来判断是否是图片,校验图片的大小等。
        boolean isLegal = false;
        for (String type : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(multipartFile.getOriginalFilename(), type)) {
                isLegal = true;
                break;  // 只要与允许上传格式其中一个匹配就可以
            }
        }
        PicUploadResult picUploadResult = new PicUploadResult();
        // 格式错误, 返回与前端约定的error
        if (!isLegal) {
            picUploadResult.setSuccess(false);
            picUploadResult.setInformation("图片格式错误,请重试");
            return picUploadResult;
        }

        // 2. 准备上传API的参数
        String fileName = this.getFileName(multipartFile.getOriginalFilename());

        // 3. 上传至阿里OSS
        try {
            ossClient.putObject(aliyunConfig.getBucketName(), fileName, new ByteArrayInputStream(multipartFile.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
            // 上传失败
            picUploadResult.setSuccess(false);
            picUploadResult.setInformation("未知失败");
            return picUploadResult;
        }

        // 上传成功
        picUploadResult.setSuccess(true);
        // 文件名(即直接访问的完整路径)
        picUploadResult.setName(aliyunConfig.getUrlPrefix() + fileName);

        return picUploadResult;
    }

    /**
     *
     */
    private String getFileName(String fileName) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "") + fileName.substring(fileName.lastIndexOf("."));
        return uuid;
    }

    public static void main(String[] args) {

    }

    public void deleteImage(String imageName) {

        ossClient.deleteObject(aliyunConfig.getBucketName(), imageName);
    }

}
