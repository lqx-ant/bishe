package com.example.bishe.service;

import com.alibaba.fastjson.JSON;
import com.example.bishe.enums.AreaEnum;
import com.example.bishe.enums.MoneyEnum;
import com.example.bishe.mapper.HouseMapper;
import com.example.bishe.mapper.ReserveMapper;
import com.example.bishe.mapper.UserMapper;
import com.example.bishe.pojo.House;
import com.example.bishe.pojo.Reserve;
import com.example.bishe.pojo.Result;
import com.example.bishe.pojo.SearchHouse;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class HouseService {

    @Autowired
    HouseMapper houseMapper;

    @Autowired
    ReserveMapper reserveMapper;

    @Autowired
    UserService userService;

    @Autowired
    CityService cityService;

    @Autowired
    RedisService redisService;

    public House getHouse(Long houseId) {
        House house = houseMapper.selectByPrimaryKey(houseId);
        house.setImgs(JSON.parseArray(house.getImg(), String.class));
        house.setUser(userService.getUserById(house.getUid()));
        house.setCityInfo(cityService.getCityById(house.getCityId()).getName());
        house.setTypeInfo(house.getType().equals(1) ? "整租" : "合租");

        return house;
    }

    public List<House> reHouse(Long uid) {
        String value = redisService.getMapValue("reHouse", String.valueOf(uid));
        List<House> result = new ArrayList<>();

        if (!StringUtils.isEmpty(value)) {
            List<Long> list = JSON.parseArray(value, Long.class);
            for (Long id : list) {
                result.add(getHouse(id));
            }

        } else {
            // 判断租房人的收入情况
            Example example = new Example(Reserve.class);
            example.createCriteria().andEqualTo("userId", uid);

            List<Reserve> reserves = reserveMapper.selectByExample(example);
            double money = 1500;

            if (reserves.size() > 0) {
                double v1 = 0;
                for (Reserve reserve : reserves) {
                    v1 += getHouse(reserve.getHouseId()).getMoney();
                }

                money = v1 / reserves.size();
            }

            List<Map<String, Object>> goodHouseIds = houseMapper.getGoodHouseIds(money - 500 > 0 ? money - 500 : 500, money + 500);
            if (goodHouseIds.size() < 6) {
                List<Long> ids = goodHouseIds.stream().map(t -> ((Integer) t.get("id")).longValue()).collect(Collectors.toList());

                List<Map<String, Object>> moreGoodHouseIds = houseMapper.getMoreGoodHouseIds(6 - goodHouseIds.size(), ids);
                goodHouseIds.addAll(moreGoodHouseIds);
            }

            List<Long> ids = goodHouseIds.stream().map(t ->((Integer) t.get("id")).longValue()).collect(Collectors.toList());

            redisService.addMapValue("reHouse", String.valueOf(uid), JSON.toJSONString(ids));

            for (Long id : ids) {
                result.add(getHouse(id));
            }
        }

        return result;
    }

    public int houseNumInCity(Long cityId) {
        Example example = new Example(House.class);
        example.createCriteria().andEqualTo("cityId", cityId);
        return houseMapper.selectByExample(example).size();
    }

    public List<House> searchHouse(SearchHouse search) {
        Example example = new Example(House.class);


        Example.Criteria criteria = example.createCriteria();

        if (search.getArea() != null && search.getArea() != 0) {
            criteria.andBetween("area", AreaEnum.getMin(search.getArea()), AreaEnum.getMax(search.getArea()));
        }

        if (search.getMoney() != null && search.getMoney() != 0) {
            criteria.andBetween("money", MoneyEnum.getMin(search.getMoney()), MoneyEnum.getMax(search.getMoney()));
        }

        if (!StringUtils.isEmpty(search.getKeywords())) {
            criteria.andLike("address", "%" + search.getKeywords() + "%");
        }

        if (search.getCityId() != null && search.getCityId() != 0) {
            criteria.andEqualTo("cityId", search.getCityId());
        }

        if (search.getType() != null && search.getType() != 0) {
            criteria.andEqualTo("type", search.getType() - 1);
        }

        if (search.getStatus() != null && search.getStatus() != 0) {
            criteria.andEqualTo("isRent", search.getStatus() - 1);
        }

        if (search.getUid() != null && search.getUid() != 0) {
            criteria.andEqualTo("uid", search.getUid());
        }

        PageHelper.startPage(search.getPage(), search.getPageSize());

        List<House> houses = houseMapper.selectByExample(example);

        for (House house : houses) {
            house.setCityInfo(cityService.getCityById(house.getCityId()).getName());
            house.setImgs(JSON.parseArray(house.getImg(), String.class));
            house.setIsRentInfo(house.getIsRent().equals(1) ? "已租出" : "未租出");
            house.setTypeInfo(house.getType().equals(1) ? "整租" : "合租");
            house.setUser(userService.getUserById(house.getUid()));
        }

        return houses;
    }

    public List<Integer> getHouseRentNum() {
        int num1 = houseMapper.getHouseIsRentNum();
        int num2 = houseMapper.getHouseIsNotRentNum();
        return new ArrayList<>(Arrays.asList(num2, num1));
    }

    public List<Integer> getHouseTypeNum() {
        int num1 = houseMapper.getHouseIsTypeNum();
        int num2 = houseMapper.getHouseIsNotTypeNum();
        return new ArrayList<>(Arrays.asList(num1, num2));

    }

    public int searchHouseCount(SearchHouse search) {
        Example example = new Example(House.class);
        Example.Criteria criteria = example.createCriteria();

        if (search.getArea() != null && search.getArea() != 0) {
            criteria.andBetween("area", AreaEnum.getMin(search.getArea()), AreaEnum.getMax(search.getArea()));
        }

        if (search.getMoney() != null && search.getMoney() != 0) {
            criteria.andBetween("money", MoneyEnum.getMin(search.getMoney()), MoneyEnum.getMax(search.getMoney()));
        }

        if (!StringUtils.isEmpty(search.getKeywords())) {
            criteria.andLike("address", "%" + search.getKeywords() + "%");
        }

        if (search.getCityId() != null && search.getCityId() != 0) {
            criteria.andEqualTo("cityId", search.getCityId());
        }

        if (search.getType() != null && search.getType() != 0) {
            criteria.andEqualTo("type", search.getType() - 1);
        }

        if (search.getStatus() != null && search.getStatus() != 0) {
            criteria.andEqualTo("isRent", search.getStatus() - 1);
        }


        return houseMapper.selectCountByExample(example);
    }

    public void save(House house) {
        if (house.getId() == null) {
            houseMapper.insertSelective(house);
        } else {
            houseMapper.updateByPrimaryKeySelective(house);
        }
    }

    public void del(Long houseId) {

        houseMapper.deleteByPrimaryKey(houseId);
    }
}
