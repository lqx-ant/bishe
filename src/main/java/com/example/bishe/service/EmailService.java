package com.example.bishe.service;

import com.example.bishe.pojo.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Service
public class EmailService {


    @Autowired
    private JavaMailSenderImpl mailSender;



    public void sendMail(Mail mail) {
        try {
            checkMail(mail);
            sendMimeMail(mail);
        } catch (Exception e) {
            mail.setStatus("fail");
            mail.setError(e.getMessage());
        }

    }

    private void checkMail(Mail mail) {
        if (StringUtils.isEmpty(mail.getTo())) {
            throw new RuntimeException("邮件收信人不能为空");
        }
        if (StringUtils.isEmpty(mail.getSubject())) {
            throw new RuntimeException("邮件主题不能为空");
        }
        if (StringUtils.isEmpty(mail.getText())) {
            throw new RuntimeException("邮件内容不能为空");
        }
    }

    private void sendMimeMail(Mail mail) {
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mailSender.createMimeMessage(), true);
            mail.setFrom(getMailSendFrom());
            messageHelper.setFrom(mail.getFrom());
            messageHelper.setTo(mail.getTo().split(","));
            messageHelper.setSubject(mail.getSubject());
            messageHelper.setText(mail.getText());
            if (!StringUtils.isEmpty(mail.getCc())) {
                messageHelper.setCc(mail.getCc().split(","));
            }
            if (!StringUtils.isEmpty(mail.getBcc())) {
                messageHelper.setCc(mail.getBcc().split(","));
            }
            if (mail.getMultipartFiles() != null) {
                for (MultipartFile multipartFile : mail.getMultipartFiles()) {
                    messageHelper.addAttachment(multipartFile.getOriginalFilename(), multipartFile);
                }
            }
            if (StringUtils.isEmpty(mail.getSentDate())) {
                mail.setSentDate(new Date());
                messageHelper.setSentDate(mail.getSentDate());
            }
            mailSender.send(messageHelper.getMimeMessage());
            mail.setStatus("ok");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Mail saveMail(Mail mail) {

        return mail;
    }

    public String getMailSendFrom() {
        return mailSender.getJavaMailProperties().getProperty("from");
    }
}
