package com.example.bishe.service;

import com.example.bishe.mapper.HistoryMapper;
import com.example.bishe.pojo.History;
import com.example.bishe.utils.DateUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class HistoryService {

    @Autowired
    HistoryMapper historyMapper;

    @Autowired
    HouseService houseService;

    public void addHistory(Long uid, Long houseId) {
        Date date = new Date();
        History history = new History();
        history.setHouseId(houseId);
        history.setUserId(uid);
        history.setTime(date);
        historyMapper.insertSelective(history);
    }

    public List<History> getHistoryList(Long uid, Integer page, Integer pageSize) {
        Example example = new Example(History.class);
        example.createCriteria().andEqualTo("userId", uid);
        PageHelper.startPage(page, pageSize);

        List<History> histories = historyMapper.selectByExample(example);

        histories.forEach(h -> {
            h.setHouse(houseService.getHouse(h.getHouseId()));
            try {
                h.setTimeInfo(DateUtils.format(h.getTime(), "yyyy-MM-dd HH:mm:ss"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        return histories;
    }

    public int getTotalByUid(Long id) {
        Example example = new Example(History.class);
        example.createCriteria().andEqualTo("userId", id);
        return historyMapper.selectCountByExample(example);
    }
}
