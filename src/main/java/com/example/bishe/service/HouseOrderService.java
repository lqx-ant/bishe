package com.example.bishe.service;

import com.example.bishe.mapper.HouseMapper;
import com.example.bishe.mapper.HouseOrderMapper;
import com.example.bishe.mapper.ReserveMapper;
import com.example.bishe.mapper.UserMapper;
import com.example.bishe.pojo.HouseOrder;
import com.example.bishe.pojo.Reserve;
import com.example.bishe.utils.DateUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HouseOrderService {

    @Autowired
    HouseOrderMapper houseOrderMapper;
    @Autowired
    HouseMapper houseMapper;

    @Autowired
    HouseService houseService;

    @Autowired
    UserMapper userMapper;


    public Boolean isRentHouse(Long userId, Long houseId) {
        Example example = new Example(HouseOrder.class);
        example.createCriteria().andEqualTo("userId", userId).andEqualTo("houseId", houseId).andEqualTo("isExpired", 0);

        List<HouseOrder> reserves = houseOrderMapper.selectByExample(example);

        return reserves.size() > 0;
    }

    public void insert(Reserve reserve) {
        HouseOrder houseOrder = new HouseOrder();
        houseOrder.setUserId(reserve.getUserId());
        houseOrder.setEndTime(reserve.getEndTime());
        houseOrder.setStartTime(reserve.getStartTime());
        houseOrder.setHouseId(reserve.getHouseId());
        houseOrderMapper.insertSelective(houseOrder);
    }

    public List<HouseOrder> getOrders(Long uid, Integer page, Integer pageSize) {
        List<Long> houseIds = houseMapper.getHouseIds(uid);

        if (houseIds == null || houseIds.size() == 0) {
            return new ArrayList<>();
        }

        Example example = new Example(HouseOrder.class);
        example.setOrderByClause("id DESC");

        example.createCriteria().andIn("houseId", houseIds);
        PageHelper.startPage(page, pageSize);

        List<HouseOrder> reserves = houseOrderMapper.selectByExample(example);

        return reserves.stream().peek(reserve -> {
            reserve.setUsername(userMapper.selectByPrimaryKey(reserve.getUserId()).getUsername());
            reserve.setHouse(houseService.getHouse(reserve.getHouseId()));
            try {
                reserve.setStartTimeInfo(DateUtils.format(reserve.getStartTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                reserve.setEndTimeInfo(DateUtils.format(reserve.getEndTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }).collect(Collectors.toList());
    }

    public int getTotal(Long uid) {
        List<Long> houseIds = houseMapper.getHouseIds(uid);

        if (houseIds == null || houseIds.size() == 0) {
            return 0;
        }

        Example example = new Example(HouseOrder.class);
        example.createCriteria().andIn("houseId", houseIds);

        return houseOrderMapper.selectCountByExample(example);
    }
}
