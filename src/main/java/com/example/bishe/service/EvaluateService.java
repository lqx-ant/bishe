package com.example.bishe.service;

import com.example.bishe.mapper.EvaluateMapper;
import com.example.bishe.pojo.Evaluate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvaluateService {

    @Autowired
    EvaluateMapper evaluateMapper;

    public void save(Evaluate evaluate) {
        evaluateMapper.insertSelective(evaluate);
    }

}
