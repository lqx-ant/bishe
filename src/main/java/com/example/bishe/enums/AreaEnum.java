package com.example.bishe.enums;

public enum AreaEnum {
    one(0,0,1000),
    two(1,0,20),
    three(2,20,40),
    four(3,40,60),
    five(4,60,80),
    six(5,80,100),
    seven(6,100,1000);

    private int id;

    private int max;

    private int min;

    AreaEnum(int id, int max, int min) {
        this.id = id;
        this.max = max;
        this.min = min;
    }

    public static int getMax(int id) {
        for (AreaEnum value : AreaEnum.values()) {
            if (value.id == id) {
                return value.max;
            }
        }
        return 0;
    }

    public static int getMin(int id) {
        for (AreaEnum value : AreaEnum.values()) {
            if (value.id == id) {
                return value.min;
            }
        }
        return 0;
    }

}
