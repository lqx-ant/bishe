package com.example.bishe.enums;


public enum MoneyEnum {

    one(0, 0, 100000),
    two(1, 0, 1000),
    three(2, 1000, 2000),
    four(3, 2000, 3000),
    five(4, 3000, 4000),
    six(5, 4000, 5000),
    seven(6, 5000, 100000);

    private int id;

    private int max;

    private int min;

    MoneyEnum(int id, int max, int min) {
        this.id = id;
        this.max = max;
        this.min = min;
    }

    public static int getMax(int id) {
        for (MoneyEnum value : MoneyEnum.values()) {
            if (value.id == id) {
                return value.max;
            }
        }
        return 0;
    }

    public static int getMin(int id) {
        for (MoneyEnum value : MoneyEnum.values()) {
            if (value.id == id) {
                return value.min;
            }
        }
        return 0;
    }
}
