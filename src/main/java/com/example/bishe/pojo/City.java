package com.example.bishe.pojo;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="city")
public class City {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;


    @Transient
    private int num;

}
