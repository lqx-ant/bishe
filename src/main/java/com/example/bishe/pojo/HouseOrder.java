package com.example.bishe.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "houseOrder")
public class HouseOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "houseId")
    private Long houseId;

    @Column(name = "startTime")
    private Date startTime;

    @Column(name = "endTime")
    private Date endTime;

    /**
     *
     * 0 没过期
     * 1 过期
     */
    @Column(name = "isExpired")
    private Integer isExpired;



    @Transient
    private String username;

    @Transient
    private House house;

    @Transient
    private String startTimeInfo;

    @Transient
    private String endTimeInfo;

}
