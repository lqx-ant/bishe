package com.example.bishe.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "history")
public class History {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "houseId")
    private Long houseId;

    @Column(name = "time")
    private Date time;

    @Transient
    private House house;

    @Transient
    private String timeInfo;
}
