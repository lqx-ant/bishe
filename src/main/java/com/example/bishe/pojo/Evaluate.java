package com.example.bishe.pojo;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "evaluate")
public class Evaluate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "v1")
    private Integer v1;

    @Column(name = "v2")
    private Integer v2;

    @Column(name = "v3")
    private Integer v3;

    @Column(name = "ev")
    private String ev;

    @Column(name = "userId")
    private Long uid;

    @Column(name = "houseId")
    private Long houseId;
}
