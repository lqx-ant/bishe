package com.example.bishe.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Table(name = "house")
public class House {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "city_id")
    private Long cityId;

    @Column(name = "address")
    private String address;

    @Column(name = "money")
    private Double money;

    @Column(name = "bedroom")
    private Integer bedroom;

    @Column(name = "area")
    private Double area;

    @Column(name = "toilet")
    private Integer toilet;

    /**
     * 1 整租
     * 0 合租
     */
    @Column(name = "type")
    private Integer type;

    @Column(name = "img")
    private String img;

    @Column(name = "uid")
    private Long uid;


    /**
     * 1 已出租
     */
    @Column(name = "isRent")
    private Integer isRent;

    /**
     * 房间号
     */
    @Column(name = "house_number")
    private String houseNumber;

    @Column(name = "introduce")
    private String introduce;


    @Transient
    private List<String> imgs = new ArrayList<>();

    @Transient
    private String cityInfo;

    @Transient
    private String typeInfo;

    @Transient
    private User user;

    @Transient
    private String isRentInfo;
}
