package com.example.bishe.pojo;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    /**
     * 1 是房东，0 是租客
     */
    @Column(name = "isLandlord")
    private Integer isLandlord;

    @Column(name = "email")
    private String email;

    @Column(name = "iphone")
    private String iphone;

    @Column(name = "img")
    private String img;


    @Transient
    private String code;

}
