package com.example.bishe.pojo;

import lombok.Data;

@Data
public class PicUploadResult {

    private boolean success;

    private String name;

    private String information;

}
