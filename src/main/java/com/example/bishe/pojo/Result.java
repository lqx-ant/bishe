package com.example.bishe.pojo;

import lombok.Data;

@Data
public class Result<T> {

    private Boolean success;
    private T data;
    private String information;


    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<>();
        result.data = data;
        result.success = true;
        return result;
    }

    public static <T> Result<T> error(String information) {
        Result<T> result = new Result<>();
        result.information = information;
        result.success = false;
        return result;
    }
}
