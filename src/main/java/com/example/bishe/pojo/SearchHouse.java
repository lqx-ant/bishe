package com.example.bishe.pojo;

import lombok.Data;

@Data
public class SearchHouse {
    String keywords;

    Integer cityId;

    Integer type;

    Integer money;

    Integer area;

    Integer status;

    Integer page = 1;

    Integer pageSize;

    Long uid;
}
