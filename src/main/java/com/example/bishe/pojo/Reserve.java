package com.example.bishe.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "reserve")
public class Reserve {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "houseId")
    private Long houseId;

    /**
     *
     * 0 未处理
     * 1 拒绝
     * 2 同意
     */
    @Column(name = "isExpired")
    private Integer isExpired;

    @Column(name = "startTime")
    private Date startTime;

    @Column(name = "endTime")
    private Date endTime;

    @Transient
    private String username;

    @Transient
    private House house;

    @Transient
    private String startTimeInfo;

    @Transient
    private String endTimeInfo;
}
