package com.example.bishe;

import net.jodah.expiringmap.ExpirationPolicy;
import net.jodah.expiringmap.ExpiringMap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableScheduling
@MapperScan(basePackages = "com.example.bishe.mapper")
@EnableSwagger2
public class BisheApplication {

    public static void main(String[] args) {
        SpringApplication.run(BisheApplication.class, args);
    }

    @Bean
    public ExpiringMap<String, String> expiringMap() {
        ExpiringMap<String, String> map = ExpiringMap.builder()
                .maxSize(100)
                .expiration(6, TimeUnit.MINUTES)
                .variableExpiration().expirationPolicy(ExpirationPolicy.CREATED).build();
        return map;
    }


}
