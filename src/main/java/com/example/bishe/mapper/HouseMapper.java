package com.example.bishe.mapper;

import com.example.bishe.pojo.House;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;


@Repository
public interface HouseMapper extends Mapper<House> {


    @Select("select count(1) from house where isRent=1")
    int getHouseIsRentNum();

    @Select("select count(1) from house where isRent=0")
    int getHouseIsNotRentNum();

    @Select("select count(1) from house where type=1")
    int getHouseIsTypeNum();

    @Select("select count(1) from house where type=0")
    int getHouseIsNotTypeNum();

    @Select("select id from house where uid=#{uid}")
    List<Long> getHouseIds(Long uid);

    @Select("select h.id ,(avg(evaluate.v1)+avg(evaluate.v2)+avg(evaluate.v3)) as score " +
            "from  (select id from house where house.money between #{min} and #{max} && isRent = 0 ) h " +
            "left join evaluate on h.id=evaluate.houseId " +
            "group by h.id " +
            "order by score desc limit 6")
    List<Map<String, Object>> getGoodHouseIds(double min, double max);

    @Select(
            "<script>" +
                    "select h.id ,(avg(evaluate.v1)+avg(evaluate.v2)+avg(evaluate.v3)) as score " +
                    "from (select id from house where isRent=0 " +
                    "<if test='list!=null and list.size()!=0'> "
                    +"and house.id not in "
                    + "  <foreach collection='list' item='item' index='index' "
                    + "    open='(' separator=',' close=')' >                 "
                    + "    #{item}                                            "
                    + "  </foreach>                                           "
                    + "</if>" +
                    ") h " +
                    "left join evaluate on h.id=evaluate.houseId " +
                    "group by h.id " +
                    "order by score desc limit ${count} " +
                    "</script>"
    )
    List<Map<String, Object>> getMoreGoodHouseIds(@Param("count") int count, @Param("list") List<Long> list);
}
