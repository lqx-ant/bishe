package com.example.bishe.mapper;

import com.example.bishe.pojo.City;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface CityMapper extends Mapper<City> {
}
