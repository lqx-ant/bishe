package com.example.bishe.mapper;

import com.example.bishe.pojo.Chat;
import com.example.bishe.pojo.City;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Set;

@Repository
public interface ChatMapper extends Mapper<Chat> {

    @Select("select distinct from_user from chat where to_user=#{userId}")
    List<Long> findFromUserByUserId(Long userId);

    @Select("select distinct to_user from chat where from_user=#{userId}")
    List<Long> findToUserByUserId(Long userId);


    @Select("select * from chat where to_user=#{userId} and from_user=#{charUserId}")
    List<Chat> findFromChatByUserId(@Param("userId") Long userId, @Param("charUserId") Long chatUserId);

    @Select("select *  from chat where from_user=#{userId} and to_user=#{charUserId}")
    List<Chat> findToChatByUserId(@Param("userId") Long userId, @Param("charUserId") Long chatUserId);

    @Select("select count(1) from chat where isRead=0 and to_user=#{userId}")
    int getUnReadNum(Long userId);

    @Update("update chat set isRead=1 where isRead=0 and to_user=#{userId}")
    int updateUnRead(Long userId);
}
