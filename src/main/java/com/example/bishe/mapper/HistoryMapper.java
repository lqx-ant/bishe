package com.example.bishe.mapper;


import com.example.bishe.pojo.History;
import com.example.bishe.pojo.User;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface HistoryMapper extends Mapper<History> {
}
