package com.example.bishe.mapper;

import com.example.bishe.pojo.Evaluate;
import com.example.bishe.pojo.House;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface EvaluateMapper extends Mapper<Evaluate> {
}
