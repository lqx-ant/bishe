package com.example.bishe.mapper;

import com.example.bishe.pojo.City;
import com.example.bishe.pojo.Reserve;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface ReserveMapper extends Mapper<Reserve> {
}
