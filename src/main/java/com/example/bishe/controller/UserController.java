package com.example.bishe.controller;

import com.example.bishe.pojo.Mail;
import com.example.bishe.pojo.Result;
import com.example.bishe.pojo.User;
import com.example.bishe.service.EmailService;
import com.example.bishe.service.UserService;
import com.example.bishe.utils.RandomUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import net.jodah.expiringmap.ExpiringMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@RequestMapping("/user")
public class UserController {


    @Autowired
    UserService userService;

    @Autowired
    private EmailService emailService;

    @Value("${spring.mail.properties.from}")
    private String emailFrom;

    @Autowired
    private ExpiringMap<String, String> expiringMap;

    @RequestMapping("/login")
    public Result<String> login(@RequestBody User user, HttpServletRequest request) {
        User useInfo = userService.getUseInfo(user.getUsername());

        if (useInfo != null && useInfo.getPassword().equals(user.getPassword())) {
            request.getSession().setAttribute("user", useInfo);
            return Result.success("success");
        } else {
            return Result.error("error");
        }
    }

    @RequestMapping("/out")
    public Result<String> release(HttpServletRequest request) {
        request.getSession().invalidate();
        return Result.success("success");
    }

    @RequestMapping("/getInfo")
    public Result<User> getUser(HttpServletRequest request) {
        return Result.success((User) request.getSession().getAttribute("user"));
    }

    @RequestMapping("/beRegister")
    public Result<String> beRegister(@RequestBody User user) {
        //判断是否注册
        if (userService.getUseInfo(user.getUsername()) != null) {
            return Result.error("账号已经被注册");
        }

        String random = RandomUtils.getRandom4();
        //发送邮件
        Mail mail = new Mail();
        mail.setSubject("在线短租平台");
        mail.setTo(user.getEmail());
        mail.setFrom(emailFrom);
        mail.setText("验证码(五分钟有效期): " + random);
        emailService.sendMail(mail);

        //设置map
        expiringMap.put(String.valueOf(user.getUsername()), random);

        //返回结果
        return Result.success("请注意邮箱接收验证邮件");
    }

    @RequestMapping("/register")
    public Result<String> register(@RequestBody User user) {
        //判断是否注册
        if (userService.getUseInfo(user.getUsername()) != null) {
            return Result.error("账号已经被注册");
        }


        String random = expiringMap.get(String.valueOf(user.getUsername()));

        if (Objects.equals(random, user.getCode())) {
            userService.insertUser(user);
            return Result.success("注册成功");
        } else {
            return Result.error("验证码错误");
        }
    }

    @RequestMapping("forget")
    public Result<String> forget(@RequestBody User user) {
        User userInfo = userService.getUseInfo(user.getUsername());
        //判断是否注册
        if (userInfo == null) {
            return Result.error("用户未注册");
        }

        if (!Objects.equals(user.getEmail(), userInfo.getEmail())) {
            return Result.error("用户信息输入错误");
        }

        String random = RandomUtils.getRandom4();
        //发送邮件
        Mail mail = new Mail();
        mail.setSubject("在线短租平台");
        mail.setTo(userInfo.getEmail());
        mail.setFrom(emailFrom);
        mail.setText("验证码(五分钟有效期): " + random);
        emailService.sendMail(mail);

        //设置map
        expiringMap.put(String.valueOf(user.getUsername()), random);

        //返回结果
        return Result.success("请注意邮箱接收验证邮件");
    }


    @RequestMapping("commit")
    public Result<String> commit(@RequestBody User user) {

        User userInfo = userService.getUseInfo(user.getUsername());
        //判断是否注册
        if (userInfo == null) {
            return Result.error("用户未注册");
        }

        String random = expiringMap.get(String.valueOf(user.getUsername()));

        if (Objects.equals(random, user.getCode())) {
            user.setId(userInfo.getId());
            userService.updateUser(user);
            return Result.success("修改成功");
        } else {
            return Result.error("验证码错误");
        }

    }

    @RequestMapping("/toBeLandlord")
    public Result<String> toBeLandlord(HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");

        if (userInfo.getIsLandlord().equals(0)) {
            userService.updateUserLandlord(userInfo.getId());
            userInfo.setIsLandlord(1);
        }
        return Result.success("success");
    }

    @RequestMapping("/update")
    public Result<String> toBeLandlord(@RequestBody User user, HttpServletRequest request) {
        userService.updateUser(user);
        request.getSession().setAttribute("user", user);
        return Result.success("success");
    }
}
