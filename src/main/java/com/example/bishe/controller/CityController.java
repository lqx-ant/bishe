package com.example.bishe.controller;

import com.example.bishe.pojo.City;
import com.example.bishe.pojo.Result;
import com.example.bishe.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    CityService cityService;

    @RequestMapping("/getCity")
    public Result<List<City>> getCity() {
        return Result.success(cityService.getAll());
    }

    @RequestMapping("/getCityWithNum")
    public Result<List<City>> getCityWithNum() {
        return Result.success(cityService.getAllWithNum());
    }

}
