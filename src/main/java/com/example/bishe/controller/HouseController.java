package com.example.bishe.controller;

import com.alibaba.fastjson.JSON;
import com.example.bishe.enums.AreaEnum;
import com.example.bishe.pojo.House;
import com.example.bishe.pojo.Result;
import com.example.bishe.pojo.SearchHouse;
import com.example.bishe.pojo.User;
import com.example.bishe.service.HouseService;
import com.example.bishe.service.RedisService;
import io.netty.util.internal.shaded.org.jctools.queues.MpscArrayQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/house")
public class HouseController {

    @Autowired
    HouseService houseService;

    @Autowired
    RedisService redisService;

    @RequestMapping("/getHouse")
    public Result<House> getHouse(Long houseId) {
        return Result.success(houseService.getHouse(houseId));
    }

    @RequestMapping("/reHouse")
    public Result<List<House>> reHouse(HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");

        return Result.success(houseService.reHouse(userInfo.getId()));
    }

    @RequestMapping("/search")
    public Result<Map> searchHouse(SearchHouse search) {
        Map<String, Object> res = new HashMap<>();

        List<House> find = houseService.searchHouse(search);
        res.put("list", find);

        res.put("count", houseService.searchHouseCount(search));

        return Result.success(res);
    }

    @RequestMapping("/getHouseInfo")
    public Result<Map<String, List<Integer>>> getHouseInfo() {
        Map<String, List<Integer>> map = new HashMap<>();
        map.put("one", houseService.getHouseRentNum());
        map.put("two", houseService.getHouseTypeNum());

        return Result.success(map);
    }

    @RequestMapping("/save")
    public Result<Boolean> save(@RequestBody House house, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");
        house.setUid(userInfo.getId());
        for (String img : house.getImgs()) {
            redisService.add(RedisService.indbKey, img);
        }
        house.setImg(JSON.toJSONString(house.getImgs()));
        houseService.save(house);
        return Result.success(true);
    }

    @RequestMapping("/del")
    public Result<Boolean> del(Long houseId) {

        houseService.del(houseId);
        return Result.success(true);
    }

}
