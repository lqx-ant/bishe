package com.example.bishe.controller;

import com.example.bishe.pojo.Chat;
import com.example.bishe.pojo.Result;
import com.example.bishe.pojo.User;
import com.example.bishe.service.ChatService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    ChatService chatService;

    @RequestMapping("/findChatUsers")
    public Result<List<User>> findChatUsers(HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");
        return Result.success(chatService.findChatUsers(userInfo.getId()));
    }

    @RequestMapping("/getChats")
    public Result<List<Chat>> getChats(Long chatUserId, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");
        return Result.success(chatService.findByUserId(userInfo.getId(), chatUserId));
    }

    @RequestMapping("/getUnReadNum")
    public Result<Integer> getUnReadNum(HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");
        return Result.success(chatService.getUnReadNum(userInfo.getId()));
    }

    @RequestMapping("/updateUnRead")
    public Result<Integer> updateUnRead(HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");
        chatService.updateUnRead(userInfo.getId());
        return Result.success(1);
    }

    @RequestMapping("sentMessage")
    public Result<String> sentMessage(String message, Long toUserId, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");
        chatService.sentMessage(userInfo.getId(), toUserId, message);



        return Result.success("success");
    }
}
