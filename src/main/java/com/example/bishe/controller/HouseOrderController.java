package com.example.bishe.controller;

import com.example.bishe.pojo.HouseOrder;
import com.example.bishe.pojo.Reserve;
import com.example.bishe.pojo.Result;
import com.example.bishe.pojo.User;
import com.example.bishe.service.HouseOrderService;
import com.example.bishe.service.ReserveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/houseOrder")
public class HouseOrderController {

    @Autowired
    HouseOrderService houseOrderService;


    @RequestMapping("/isRentHouse")
    public Result<Boolean> isReServeHouse(Long userId, Long houseId) {
        return Result.success(houseOrderService.isRentHouse(userId, houseId));
    }

    @RequestMapping("/getOrders")
    public Result<Map<String, Object>> getReserves(Integer page, Integer pageSize, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");

        List<HouseOrder> list = houseOrderService.getOrders(userInfo.getId(), page, pageSize);
        int total = houseOrderService.getTotal(userInfo.getId());

        Map<String, Object> map = new HashMap<>();

        map.put("data", list);
        map.put("total", total);

        return Result.success(map);
    }

}
