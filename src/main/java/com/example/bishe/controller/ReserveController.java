package com.example.bishe.controller;

import com.example.bishe.pojo.Reserve;
import com.example.bishe.pojo.Result;
import com.example.bishe.pojo.User;
import com.example.bishe.service.ChatService;
import com.example.bishe.service.ReserveService;
import com.example.bishe.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/reserve")
public class ReserveController {

    @Autowired
    ReserveService reserveService;

    @Autowired
    ChatService chatService;


    @RequestMapping("/isReServeHouse")
    public Result<Boolean> isReServeHouse(Long userId, Long houseId) {
        return Result.success(reserveService.isReServeHouse(userId, houseId));
    }

    @RequestMapping("/reserveHouse")
    public Result<Boolean> reserveHouse(Long houseId, String startTime, String endTime, HttpServletRequest request) throws ParseException {
        User userInfo = (User) request.getSession().getAttribute("user");

        reserveService.reserveHouse(userInfo.getId(), houseId, DateUtils.parse(startTime), DateUtils.parse(endTime));
        return Result.success(true);
    }

    @RequestMapping("/getReserves")
    public Result<Map<String, Object>> getReserves(Integer page, Integer pageSize, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");

        List<Reserve> list = reserveService.getReserves(userInfo.getId(), page, pageSize);
        int total = reserveService.getTotal(userInfo.getId());

        Map<String, Object> map = new HashMap<>();

        map.put("data", list);
        map.put("total", total);

        return Result.success(map);
    }

    @RequestMapping("/agree")
    public void agree(Long reserveId) {
        reserveService.agree(reserveId);
    }

    @RequestMapping("/decline")
    public void decline(Long reserveId) {
        reserveService.decline(reserveId);
    }


    @RequestMapping("/getReservesByUid")
    public Result<Map<String, Object>> getReservesByUid(Integer page, Integer pageSize, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");

        List<Reserve> list = reserveService.getReservesByUid(userInfo.getId(), page, pageSize);
        int total = reserveService.getTotalByUid(userInfo.getId());

        Map<String, Object> map = new HashMap<>();

        map.put("data", list);
        map.put("total", total);

        return Result.success(map);
    }
}
