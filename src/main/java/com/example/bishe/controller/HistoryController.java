package com.example.bishe.controller;

import com.example.bishe.pojo.History;
import com.example.bishe.pojo.Reserve;
import com.example.bishe.pojo.Result;
import com.example.bishe.pojo.User;
import com.example.bishe.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/history")
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    @RequestMapping("/addHistory")
    public void addHistory(Long houseId, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");

        historyService.addHistory(userInfo.getId(), houseId);
    }

    @RequestMapping("/getHistory")
    public Result<Map<String, Object>> getHistory(Integer page, Integer pageSize, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");

        List<History> list = historyService.getHistoryList(userInfo.getId(), page, pageSize);
        int total = historyService.getTotalByUid(userInfo.getId());

        Map<String, Object> map = new HashMap<>();

        map.put("data", list);
        map.put("total", total);

        return Result.success(map);
    }


}
