package com.example.bishe.controller;

import com.example.bishe.pojo.PicUploadResult;
import com.example.bishe.pojo.Result;
import com.example.bishe.service.RedisService;
import com.example.bishe.service.UploadPicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("upload")
public class UploadController {

    @Autowired
    UploadPicService uploadPicService;

    @Autowired
    RedisService redisService;




    @PostMapping("/userImg")
    public Result<String> upload(@RequestParam("file") MultipartFile file) {

        PicUploadResult upload = uploadPicService.upload(file);

        if(upload.isSuccess()){
            redisService.add(RedisService.uploadKey,upload.getName());
            return Result.success(upload.getName());
        }else {
            return Result.error(upload.getInformation());
        }
    }

}
