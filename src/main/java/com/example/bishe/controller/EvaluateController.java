package com.example.bishe.controller;

import com.example.bishe.pojo.Evaluate;
import com.example.bishe.pojo.User;
import com.example.bishe.service.EvaluateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/evaluate")
public class EvaluateController {

    @Autowired
    EvaluateService evaluateService;

    @RequestMapping("/save")
    public void save(Evaluate evaluate, HttpServletRequest request) {
        User userInfo = (User) request.getSession().getAttribute("user");
        evaluate.setUid(userInfo.getId());
        evaluateService.save(evaluate);
    }
}
