package com.example.bishe.config;

import com.example.bishe.mapper.HouseOrderMapper;
import com.example.bishe.pojo.HouseOrder;
import com.example.bishe.service.RedisService;
import com.example.bishe.service.UploadPicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.Date;

@Configuration
public class TaskConfig {


    @Autowired
    HouseOrderMapper houseOrderMapper;

    @Autowired
    RedisService redisService;

    @Autowired
    UploadPicService uploadPicService;

    @Autowired
    AliyunConfig aliyunConfig;

    /**
     * 每天凌晨 清理图片
     */
    @Scheduled(cron = "0 0 0 * * ? ")
    public void clearImage() {
        for (String s : redisService.diff(RedisService.uploadKey, RedisService.indbKey)) {
            uploadPicService.deleteImage(s.substring(aliyunConfig.getUrlPrefix().length()));
        }
    }

    /**
     * 每天凌晨 处理退租手续
     */
    @Scheduled(cron = "0 0 0 * * ? ")
    public void clearHouse() {
        Date date = new Date();
        for (HouseOrder houseOrder : houseOrderMapper.selectAll()) {
            if (houseOrder.getEndTime().before(date)) {
                houseOrder.setIsExpired(1);
                houseOrderMapper.updateByPrimaryKeySelective(houseOrder);
            }
        }
    }
}
