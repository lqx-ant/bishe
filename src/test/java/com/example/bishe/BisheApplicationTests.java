package com.example.bishe;

import com.aliyun.oss.OSSClient;
import com.example.bishe.config.AliyunConfig;
import com.example.bishe.mapper.HouseMapper;
import com.example.bishe.service.UploadPicService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.Map;

@SpringBootTest
class BisheApplicationTests {

    @Autowired
    private OSSClient ossClient;

    @Autowired
    private AliyunConfig aliyunConfig;

    @Autowired
    private HouseMapper houseMapper;


    @Test
    void contextLoads() {

        for (Map<String, Object> goodHouseId : houseMapper.getGoodHouseIds(722, 1722)) {
            System.out.println(goodHouseId);
        }

        houseMapper.getMoreGoodHouseIds(4, Arrays.asList(6L, 9L));
    }

}
