create table houseorder
(
    id        int auto_increment
        primary key,
    userId    int default 0 not null,
    houseId   int default 0 not null,
    startTime datetime      not null,
    endTime   datetime      not null,
    isExpired int default 0 null
)
    engine = InnoDB;

INSERT INTO bishe.houseorder (id, userId, houseId, startTime, endTime, isExpired) VALUES (1, 1, 2, '1970-01-01 00:00:00', '2018-03-01 00:45:40', 1);
INSERT INTO bishe.houseorder (id, userId, houseId, startTime, endTime, isExpired) VALUES (2, 3, 7, '2022-05-02 20:49:15', '2022-03-02 20:49:22', 1);
INSERT INTO bishe.houseorder (id, userId, houseId, startTime, endTime, isExpired) VALUES (3, 4, 6, '2022-03-02 00:00:00', '2022-04-11 00:00:00', 1);
