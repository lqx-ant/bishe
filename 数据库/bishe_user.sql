create table user
(
    id         int auto_increment comment 'id'
        primary key,
    password   varchar(255) default '' null comment '用户密码',
    isLandlord int(1)       default 0  null comment '房东',
    email      varchar(255) default '' null comment '邮箱',
    username   varchar(255) default '' null,
    iphone     varchar(26)  default '' null,
    img        varchar(255) default '' null
)
    comment '用户表' engine = InnoDB;

INSERT INTO bishe.user (id, password, isLandlord, email, username, iphone, img) VALUES (1, '1234', 1, '903437991@qq.com', 'leshi', '12347899999', 'https://bishe-lqx.oss-cn-beijing.aliyuncs.com/95e7eb6f20d24d9fbe18df5f93f0c9c3.jpg');
INSERT INTO bishe.user (id, password, isLandlord, email, username, iphone, img) VALUES (2, '1234', 1, '903437991@qq.com', 'ssfa12', '123456789', 'https://bishe-lqx.oss-cn-beijing.aliyuncs.com/7255bd78d93a41e1b7481e506be8cba7.jpg');
INSERT INTO bishe.user (id, password, isLandlord, email, username, iphone, img) VALUES (3, '1234', 1, '903437991@qq.com', 'lqx', '18856359150', 'https://bishe-lqx.oss-cn-beijing.aliyuncs.com/7255bd78d93a41e1b7481e506be8cba7.jpg');
INSERT INTO bishe.user (id, password, isLandlord, email, username, iphone, img) VALUES (4, '1234', 0, '903437991@qq.com', 'www', '1323', 'https://bishe-lqx.oss-cn-beijing.aliyuncs.com/7255bd78d93a41e1b7481e506be8cba7.jpg');
INSERT INTO bishe.user (id, password, isLandlord, email, username, iphone, img) VALUES (5, '1234', 0, '903437991@qq.com', 'wtf', '18856359150', 'https://bishe-lqx.oss-cn-beijing.aliyuncs.com/48d73d96a86a4ba5b0ca436bb277b316.jpg');
