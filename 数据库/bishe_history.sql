create table history
(
    id      int auto_increment
        primary key,
    userId  int      default 0                     not null,
    houseId int      default 0                     not null,
    time    datetime default '0000-00-00 00:00:00' null
)
    engine = InnoDB;

INSERT INTO bishe.history (id, userId, houseId, time) VALUES (1, 1, 9, '2022-05-02 20:09:07');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (2, 1, 9, '2022-05-02 20:11:34');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (3, 1, 6, '2022-05-02 21:11:56');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (4, 1, 6, '2022-05-02 21:12:20');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (5, 1, 9, '2022-05-02 21:12:26');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (6, 1, 8, '2022-05-02 21:12:32');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (7, 1, 8, '2022-05-02 21:12:39');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (8, 1, 2, '2022-05-03 11:51:32');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (9, 1, 9, '2022-05-03 11:51:35');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (10, 1, 6, '2022-05-06 17:24:53');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (11, 1, 6, '2022-05-06 17:30:28');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (12, 1, 2, '2022-05-06 17:40:41');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (13, 1, 6, '2022-05-06 17:40:49');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (14, 1, 6, '2022-05-06 18:00:13');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (15, 1, 2, '2022-05-07 11:21:54');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (16, 1, 2, '2022-05-19 12:22:24');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (17, 1, 6, '2022-05-19 12:22:27');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (18, 1, 6, '2022-05-19 12:22:32');
INSERT INTO bishe.history (id, userId, houseId, time) VALUES (19, 1, 1, '2022-05-19 12:22:35');
