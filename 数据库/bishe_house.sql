create table house
(
    id           int auto_increment
        primary key,
    city_id      int           default 0  not null,
    address      varchar(255)  default '' not null,
    money        double        default 0  not null,
    bedroom      int           default 0  not null,
    area         double        default 0  not null,
    toilet       int           default 0  not null,
    type         int           default 0  not null,
    img          varchar(1024) default '' not null,
    uid          int           default 0  null,
    isRent       int           default 0  null,
    introduce    varchar(255)  default '' null,
    house_number varchar(255)  default '' null comment '房间号'
)
    engine = InnoDB;

INSERT INTO bishe.house (id, city_id, address, money, bedroom, area, toilet, type, img, uid, isRent, introduce, house_number) VALUES (1, 1, '华策中心城 五号线 吉祥站 花园小区', 3000, 1, 100, 0, 1, '["https://bishe-lqx.oss-cn-beijing.aliyuncs.com/d7c147eff9e8420d88a1e8e4df05e0ba.jpg","https://bishe-lqx.oss-cn-beijing.aliyuncs.com/90107bd231b14586bb0abac76a69762b.jpg"]', 2, 0, '此房户型方正，采光好。周边配套齐全，交通方便。 小区邻近三号线吉祥站，楼下公交站台林立。 带全套家电。', '13栋2单元501室');
INSERT INTO bishe.house (id, city_id, address, money, bedroom, area, toilet, type, img, uid, isRent, introduce, house_number) VALUES (2, 1, '华策中心城 五号线 吉祥站 花园小区', 2500, 1, 100, 0, 1, '["https://bishe-lqx.oss-cn-beijing.aliyuncs.com/3ca5321e8a5d4d60bcc3bed194603d27.jpg","https://bishe-lqx.oss-cn-beijing.aliyuncs.com/540d0cb30dc34f3a80f05318b19f7986.jpg"]', 2, 0, '此房户型方正，采光好。周边配套齐全，交通方便。 小区邻近三号线吉祥站，楼下公交站台林立。 带全套家电。', '13栋2单元501室');
INSERT INTO bishe.house (id, city_id, address, money, bedroom, area, toilet, type, img, uid, isRent, introduce, house_number) VALUES (6, 3, '华策中心城 三号线 吉祥站 花园小区', 3500, 2, 200, 1, 0, '["https://bishe-lqx.oss-cn-beijing.aliyuncs.com/house.png","https://bishe-lqx.oss-cn-beijing.aliyuncs.com/ce5ab6e83f634f27898409670a3568ab.jpg"]', 1, 0, '此房户型方正，采光好。周边配套齐全，交通方便。 小区邻近三号线吉祥站，楼下公交站台林立。 带全套家电。', '13栋2单元501室');
INSERT INTO bishe.house (id, city_id, address, money, bedroom, area, toilet, type, img, uid, isRent, introduce, house_number) VALUES (7, 3, '百花街道 海伦国际 阳光花园', 6000, 3, 200, 2, 1, '["https://bishe-lqx.oss-cn-beijing.aliyuncs.com/413ef30be3a34b50b936af9acd660405.jpg","https://bishe-lqx.oss-cn-beijing.aliyuncs.com/b80a794e71474b29aa6b10e74d76e84d.jpg"]', 1, 0, '装修华丽，环境优美，周围娱乐设置齐全，此房户型方正，采光好。周边配套齐全，交通方便。楼下公交站台林立。 带全套家电。', '14栋楼3单元312号');
INSERT INTO bishe.house (id, city_id, address, money, bedroom, area, toilet, type, img, uid, isRent, introduce, house_number) VALUES (8, 1, '学院路西段 聚贤雅丽园 桂湖名城', 1500, 2, 50, 1, 0, '["https://bishe-lqx.oss-cn-beijing.aliyuncs.com/6d80f6e7e0b64148a6d246160c3dd7ef.jpg","https://bishe-lqx.oss-cn-beijing.aliyuncs.com/9a733b5340214ee99948d52582456ceb.jpg"]', 1, 0, '此房户型方正，采光好。周边配套齐全，交通方便。 小区邻近地铁站，楼下公交站台林立。 带全套家电。', '2栋3单元501室');
INSERT INTO bishe.house (id, city_id, address, money, bedroom, area, toilet, type, img, uid, isRent, introduce, house_number) VALUES (9, 1, '学院路东段 上锦康城 团结二区', 1600, 1, 12, 1, 0, '["https://bishe-lqx.oss-cn-beijing.aliyuncs.com/52abfebce052466da91e13a057d3e4c7.jpg","https://bishe-lqx.oss-cn-beijing.aliyuncs.com/eaffa9c4ca4841fc9cd8e7d5a853af3a.jpg"]', 1, 0, '此房户型方正，采光好。周边配套齐全，交通方便。 小区邻近交通高铁站，楼下公交站台林立。 带全套家电。', '21栋2单元501室');
