create table reserve
(
    id        int auto_increment
        primary key,
    userId    int default 0 not null,
    houseId   int default 0 not null,
    isExpired int default 0 null,
    startTime datetime      null,
    endTime   datetime      null
)
    engine = InnoDB;

INSERT INTO bishe.reserve (id, userId, houseId, isExpired, startTime, endTime) VALUES (2, 4, 1, 0, '2022-03-18 00:00:00', '2022-03-24 00:00:00');
INSERT INTO bishe.reserve (id, userId, houseId, isExpired, startTime, endTime) VALUES (3, 2, 7, 2, '2022-05-02 20:49:15', '2022-03-02 20:49:22');
INSERT INTO bishe.reserve (id, userId, houseId, isExpired, startTime, endTime) VALUES (4, 3, 7, 1, '2022-05-02 20:49:15', '2022-03-02 20:49:22');
INSERT INTO bishe.reserve (id, userId, houseId, isExpired, startTime, endTime) VALUES (5, 4, 6, 1, '2022-03-02 00:00:00', '2022-04-11 00:00:00');
INSERT INTO bishe.reserve (id, userId, houseId, isExpired, startTime, endTime) VALUES (6, 1, 9, 2, '2022-04-15 00:00:00', '2022-04-22 00:00:00');
INSERT INTO bishe.reserve (id, userId, houseId, isExpired, startTime, endTime) VALUES (7, 1, 1, 0, '2022-05-04 00:00:00', '2022-05-12 00:00:00');
INSERT INTO bishe.reserve (id, userId, houseId, isExpired, startTime, endTime) VALUES (8, 1, 8, 0, '2022-05-03 00:00:00', '2022-05-10 00:00:00');
INSERT INTO bishe.reserve (id, userId, houseId, isExpired, startTime, endTime) VALUES (9, 1, 6, 0, '2022-05-02 00:00:00', '2022-05-02 00:00:00');
