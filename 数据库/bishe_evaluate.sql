create table evaluate
(
    id      int auto_increment
        primary key,
    userId  int          default 0  not null,
    houseId int          default 0  not null,
    v1      int          default 0  not null,
    v2      int          default 0  not null,
    v3      int          default 0  not null,
    ev      varchar(255) default '' not null
)
    engine = InnoDB;

INSERT INTO bishe.evaluate (id, userId, houseId, v1, v2, v3, ev) VALUES (1, 4, 6, 5, 4, 5, 'good');
INSERT INTO bishe.evaluate (id, userId, houseId, v1, v2, v3, ev) VALUES (2, 4, 6, 4, 2, 2, 'laji');
INSERT INTO bishe.evaluate (id, userId, houseId, v1, v2, v3, ev) VALUES (3, 4, 1, 3, 3, 3, 'bad');
