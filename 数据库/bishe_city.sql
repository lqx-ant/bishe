create table city
(
    id   int auto_increment
        primary key,
    name varchar(255) default '' null
)
    engine = InnoDB;

INSERT INTO bishe.city (id, name) VALUES (1, '上海');
INSERT INTO bishe.city (id, name) VALUES (2, '广州');
INSERT INTO bishe.city (id, name) VALUES (3, '北京');
INSERT INTO bishe.city (id, name) VALUES (4, '杭州');
INSERT INTO bishe.city (id, name) VALUES (5, '成都');
INSERT INTO bishe.city (id, name) VALUES (6, '南京');
INSERT INTO bishe.city (id, name) VALUES (7, '深圳');
INSERT INTO bishe.city (id, name) VALUES (8, '长沙');
INSERT INTO bishe.city (id, name) VALUES (9, '天津');
INSERT INTO bishe.city (id, name) VALUES (10, '济南');
INSERT INTO bishe.city (id, name) VALUES (11, '宣城');
INSERT INTO bishe.city (id, name) VALUES (12, '合肥');
INSERT INTO bishe.city (id, name) VALUES (13, '桂林');
INSERT INTO bishe.city (id, name) VALUES (14, '青岛');
