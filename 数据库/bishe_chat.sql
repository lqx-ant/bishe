create table chat
(
    id        int auto_increment
        primary key,
    from_user int                     not null,
    message   varchar(255) default '' not null,
    to_user   int                     not null,
    sendTime  datetime                not null,
    isRead    int          default 0  not null
)
    engine = InnoDB;

INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (1, 4, '你好,我想预约从2022-03-18 - 2022-03-24的房子', 1, '2022-03-01 01:23:39', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (2, 1, 'haode', 4, '2022-03-01 01:48:26', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (3, 1, 'dsfdsfds', 4, '2022-03-01 01:49:03', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (4, 1, 'dasdsa', 4, '2022-03-01 01:49:13', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (5, 1, 'dasdsa', 4, '2022-03-01 01:49:34', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (6, 1, 'dasdsa', 4, '2022-03-01 01:49:36', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (7, 1, 'asdas', 4, '2022-03-01 01:49:43', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (8, 4, '你好,我想预约从2022-03-02 - 2022-04-11的房子', 1, '2022-03-02 21:15:30', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (9, 1, '你好,我想预约从2022-04-15 - 2022-04-22的房子', 1, '2022-04-15 15:18:24', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (10, 1, 'ok', 4, '2022-04-20 15:59:43', 0);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (11, 1, 'ok', 4, '2022-04-20 16:34:26', 0);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (12, 1, '你好,我想预约从2022-05-04 - 2022-05-12的房子', 2, '2022-05-02 18:33:08', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (13, 1, '你好,我想预约从2022-05-03 - 2022-05-10的房子', 1, '2022-05-02 21:12:36', 1);
INSERT INTO bishe.chat (id, from_user, message, to_user, sendTime, isRead) VALUES (14, 2, '好的', 1, '2022-05-07 11:58:08', 0);
